class Alat {
  String? AltKode;
  String? AltNama;
  String? AltJumlah;

  Alat({
    this.AltKode,
    this.AltNama,
    this.AltJumlah,
  });

  factory Alat.fromJson(Map<String, dynamic> json) => Alat(
        AltKode: json['AltKode'],
        AltNama: json['AltNama'],
        AltJumlah: json['AltJumlah'],
      );

  Map<String, dynamic> toJson() => {
        'AltKode': this.AltKode,
        'AltNama': this.AltNama,
        'AltJumlah': this.AltJumlah,
      };
}
