class Peminjaman {
  String? pgmKodedetail;
  String? pgmKode;
  String? pgmKodebarang;
  String? pgmJumlah;
  Peminjaman({
    this.pgmKodedetail,
    this.pgmKode,
    this.pgmKodebarang,
    this.pgmJumlah,
  });

  factory Peminjaman.fromJson(Map<String, dynamic> json) => Peminjaman(
        pgmKodedetail: json['pgmKodedetail'],
        pgmKode: json['pgmKode'],
        pgmKodebarang: json['pgmKodebarang'],
        pgmJumlah: json['pgmJumlah'],
      );

  Map<String, dynamic> toJson() => {
        'pgmKodedetail': this.pgmKodedetail,
        'pgmKode': this.pgmKode,
        'pgmKodebarang': this.pgmKodebarang,
        'pgmJumlah': this.pgmJumlah,
      };
}
