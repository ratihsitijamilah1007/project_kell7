class Peminjaman {
  String? pjmKode;
  String? pjmTanggal;
  String? pjmNpm;
  String? pjmNama;
  String? pjmProdi;
  String? pjmNohp;
  Peminjaman({
    this.pjmKode,
    this.pjmTanggal,
    this.pjmNpm,
    this.pjmNama,
    this.pjmProdi,
    this.pjmNohp,
  });

  factory Peminjaman.fromJson(Map<String, dynamic> json) => Peminjaman(
        pjmKode: json['pjmKode'],
        pjmTanggal: json['pjmTanggal'],
        pjmNpm: json['pjmNpm'],
        pjmNama: json['pjmNama'],
        pjmProdi: json['pjmProdi'],
        pjmNohp: json['pjmNohp'],
      );

  Map<String, dynamic> toJson() => {
        'pjmKode': this.pjmKode,
        'pjmTanggal': this.pjmTanggal,
        'pjmNpm': this.pjmNpm,
        'pjmNama': this.pjmNama,
        'pjmProdi': this.pjmProdi,
        'pjmNohp': this.pjmNohp,
      };
}
