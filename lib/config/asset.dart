import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Asset {
  static Color colorPrimaryDark = Color.fromARGB(255, 8, 154, 165);
  static Color colorPrimary = Color.fromARGB(255, 199, 154, 29);
  static Color colorSecondary = Color.fromARGB(255, 0, 0, 0);
  static Color colorAccent = Color.fromARGB(255, 0, 0, 0);
}
