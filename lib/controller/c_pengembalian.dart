import 'package:get/get.dart';
import 'package:project_kel_7/model/pengembalian.dart';

class CPengembalian extends GetxController {
  Rx<CPengembalian> _pengembalian = Pengembalian().obs;

  CPengembalian get user => _pengembalian.value;

  void setPengembalian(CPengembalian dataPengembalian) => _pengembalian.value = dataPengembalian;
  
  static Pengembalian() {}
}
