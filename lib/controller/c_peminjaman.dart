import 'package:get/get.dart';
import 'package:project_kel_7/model/peminjaman.dart';

class CPeminjaman extends GetxController {
  Rx<Peminjaman> _peminjaman = Peminjaman().obs;

  Peminjaman get user => _peminjaman.value;

  void setPeminjaman(Peminjaman dataPeminjaman) => _peminjaman.value = dataPeminjaman;
}
