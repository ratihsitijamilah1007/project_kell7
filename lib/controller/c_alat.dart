import 'package:get/get.dart';
import 'package:project_kel_7/model/alat.dart';

class CAlat extends GetxController {
  Rx<Alat> _alat = Alat().obs;

  Alat get user => _alat.value;

  void setAlat(Alat dataAlat) => _alat.value = dataAlat;
}
